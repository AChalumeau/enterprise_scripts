<h1 align='center'> enterprise_scripts </h1>



We recommand to run analysis using the singularity image from https://gitlab.in2p3.fr/epta/docker_singularity/-/tree/15-simplify-image

## 1. How to launch analysis

- Single-pulsar noise analysis: `src/single_psr_analysis.py`
  - Give input data (`--pathdirdata`) & output (`--output`) folders
  - Set the noise model with `--model` as explained in [Single-pulsar signal models](#single-pulsar-signal-models)
  - Give the pulsar name as argument.
  - See `--help` for other options
  - For HyperModel:
    - Separate models with '/' `--model` to compare two or more noise models.

---

- Multi-pulsar noise analysis: `src/multi_psr_analysis.py`
  - Give input data (`--pathdirdata`) & output (`--output`) folders
  - Set the single-pulsar noise models with a single-pulsar noise json file (`--psrmodelfile`) that contains a dictionnary with pulsar names as keys and noise models as values.
  - Set the common signal model with `--commonmodel` as explained in [Common signal models](#common-signal-models).
  - Give the directory of noisefiles (`--noisedir`) to set constant values if there are.
  - See `--help` for other options
  - For HyperModel:
    - Give several psrmodel json files in `--psrmodelfile` to compare two or more single-pulsar noise models. All files must contain the same pulsar list.
    - Separate models with '/' in `--commonmodel` to compare two or more common signal models.
    - If you add several single-psr AND common models, set the same number of models for both. The n-th single-psr model corresponds to the n-th common signal model.

## 2. Examples

- Single-pulsar noise analysis in local machine with singularity, with a marginalized timing model, white noise (Efac+TNEquad), Gaussian process red noise (30 frequency bins) and DM variations (100 freq. bins):

```
singularity exec <container.sif> python3 src/single_psr_analysis.py -o playground/examples/outdir/ -p playground/examples/toas/ -m TM,WN,RN_nb-30,RN_idx-2_nb-100 J1600-3053
```

---

- Multi-pulsar noise analysis with a Common-red signal with HD ORF, in local machine with singularity:

```
singularity exec <container.sif> python3 src/multi_psr_analysis.py -o playground/examples/outdir/ -p playground/examples/toas/ -m playground/examples/psrmodels/J1600-3053_vanilla_psrmodels.json --noisedir playground/examples/noisefiles/ -c CRS_orf-hd
```



## 3. To use in a cluster

- Set your "submit file", as the ones in cluster_launcher
- Add your singularity command, the path of your submission file and the command to launch the run in cluster_launcher/initialize_run.py
- Run cluster_launcher/initialize_run.py. For the specific argument flags, replace "-" with ":", see example below.

- Examples

  1) Single-pulsar noise analysis with singularity:

     ```
     singularity exec <container.sif> python3 cluster_launcher/initialize_run.py --compname <your-cluster-name> -a :o playground/examples/outdir/J1600-3053_RN30_DM100/ptmcmc_N_1e6 :p playground/examples/toas/ :m TM,WN,RN_nb-30,RN_idx-2_nb-100 J1600-3053
     ```

  2) Multi-pulsar noise analysis with singularity:

     ``` 
     singularity exec <container.sif> python3 cluster_launcher/initialize_run.py -t multi -a :o playground/examples/outdir/multi-psr_search/ptmcmc_N_1e6 :p playground/examples/toas/ :m playground/examples/psrmodels/J1600-3053_vanilla_psrmodels.json ::noisedir playground/examples/noisefiles/ :c CRS_orf-hd"
     ```

     

## 4. Write signal models with a single string

### General:

- Add signals separated with ','. 
  - Ex: TM,WN --> Timing model marginalized + sampled white noise (EFAC+TNEquad)
- For each signal, define options separated with '_'. 
  - Ex: TM,WN_fix --> Timing model marginalized + fixed white noise
- An option could be a boolean. If not, separate the option name and value with '-'.
  - Ex: TM,WN,RN_nb-30 --> Timing model marginalized + sampled white noise (EFAC+TNEquad) + achromatic red noise with 30 freq. bins.
- To use HyperModel to compare 2 or more models: Separate models with '/'
  - Ex: TM,WN,RN_nb-30/TM,WN,RN_nb-30,RN_nb-100_idx-2 --> Idem vs. Idem + DM variations (a chromatic red noise with a chromatic index at '-idx', with idx=2).

---

### Single-pulsar signal models

See `singularity exec <container.sif> python3 src/single_psr_analysis.py -h`

- Timing model: `TM`

  - `fit`: Fit for TM. Marginalize if not (default=False).
  - `par`: Specify sub-list of parameters to fit. All parameters used if not specified (default=None).
  - `sig`: Factor used to define prior range of TM params. Prior = U(-sig * 5, sig * 5), (default=1).

  Ex: TM_par-F0-F1-DM-DM1-DM2 --> Fit for chosen timing model parameters

- White noise: `WN`. Temponest wn type: (Efac * ToAerr) ** 2 + Equad ** 2

  - `fix`: Fix WN parameters to noisedir values (default=False).

  Ex: WN_fix --> Fix white noise parameters to values given in noisefiles.

- Gaussian process red noise: `RN`

  - `nb`: Number of freq. bins (default: 30).
  - `idx`: Chromaticity. None for achromatic, 2 for DM var., 4 for scattering var., free for free-chromatic noise (prior=U(0,10)), (default=None).
  - `psd`: Choose the type of psd. pl for simple powerlaw, bpl for broken-power law, fs for free-spectrum (default=pl).
  - `log`: Logspaced bins (default=None).
  - `dp`: Dropout (only available for powerlaw psd), (default=None).
  - `fn` & `fv`: Flag name and value. Used to only select ToAs with particular flags (e.g., for Band/System noise), (default=None).

  Ex: RN_nb-30_psd-fs --> Achromatic red noise with a free-spectrum psd

- Exponential dip: `E`

  - `ti` & `tf` : Initial and final epochs.
  - `idx`: Chromaticity. None for 2 (DM), other number to fix, free for free parameter (prior=U(0,10)), (default=2)

  Ex: E_ti-55000_tf-57000 --> DM event happening from MJD 55000 to MJD 57000.

- Yearly chromatic noise: `Y`

  - `idx`: Chromaticity. None for 2 (DM), other number to fix, free for free parameter (prior=U(0,10)), (default=2)

  Ex: Y_idx-free --> Annual chromatic signal with a chromaticity set as a free-parameter.

---

### Common signal models

See `singularity exec <container.sif> python3 src/multi_psr_analysis.py -h`

- Gaussian process Common Red Signal: `CRS`
  - `psd`: Choose the type of psd. pl for simple powerlaw, bpl for broken-power law, fs for free-spectrum.
  - `nb`: Number of freq. bins (default: 30).
  - `orf`: Overlap reduction function. None for CURN (default: None). See below for the full list.
  - `log`: Logspaced bins.
  - `dp`: Dropout (only available for powerlaw psd), (default=None).
  - `dp_psr`: Give pulsar name and include `dp` to add a dropout parameter only related with the selected pulsar.

Ex: CRS_nb-30_log --> Common red signal with 30 freq. bins and log-distributed frequencies

- BayesEphem SSE error model: `BFM`

  --> Includes masses of Jup,Sat,Ura,Nep, orbital elements of Jup,Sat and a rotation (drift) rate of the SSE frame

- EphemGP SSE error model: `EGP` (Available soon)

  - `bod`: Includes orbital elements of chosen bodies (default: 'Jup,Sat')

  --> Available bodies: 

  - Jupiter (Jup)
  - Saturn (Sat)
  - Uranus (Ura)
  - Neptune (Nep)
  - Earth-Moon barycenter (EMB)
  - Mars (Mar)
  - Mercury (Mer)
  - Venus (Ven)

​	Ex: EGP_bod-Jup-Sat --> EphemGP with Jupiter & Saturn orbital elements

---

**Available ORFs for the CRS:**

- None or 'crn': CURN

- 'dipole': Dipole

- 'monopole': Monopole

- 'hd': Hellings-Downs.

  

- 'bin_orf': Agnostic binned spatial correlation function. Bin edges are placed at edges and across angular separation space.
  'chebyshev_orf': Chebyshev polynomial decomposition of the spatial correlation function.

- 'spline_orf': Agnostic spline-interpolated spatial correlation function. Spline knots are placed at edges, zeros, and minimum of H&D curve.

- 'legendre_orf': Legendre polynomial spatial correlation function. Assumes process normalization such that autocorrelation signature is 1.

  

- 'zero_diag_hd': Off-diagonal Hellings & Downs spatial correlation function. To be used in a "split likelihood" model with an additional common uncorrelated red process. The latter is necessary to regularize the overall Phi covariance matrix.

- 'zero_diag_bin_orf': Agnostic binned spatial correlation function. To be used in a "split likelihood" model with an additional common uncorrelated red process. The latter is necessary to regularize the overall Phi covariance matrix.

- 'zero_diag_legendre_orf': Legendre polynomial spatial correlation function. To be used in a "split likelihood" model with an additional common uncorrelated red process. The latter is necessary to regularize the overall Phi covariance matrix.

  

- 'param_hd': Pre-factor parametrized Hellings & Downs spatial correlation function (Taylor 2020).

- 'gw_monopole': GW-Monopole (Laal 2020).

- 'gw_dipole': GW-Dipole (Laal 2020).

- 'freq_hd': Frequency-dependent Hellings & Downs spatial correlation function. Implemented as a model that only enforces H&D inter-pulsar correlations after a certain number of frequencies in the spectrum. The first set of frequencies are uncorrelated.

- 'st': Scalar tensor correlations as induced by the breathing polarization mode of gravity (Laal 2020).

- 'gt': General Transverse (GT) Correlations. This ORF is used to detect the relative significance of all possible correlation patterns induced by the most general family of transverse gravitational waves (Laal 2020).

  

## 5. Miscellaneous

- See git branch & commit: 

```
git branch ; git log -1
```

---

- Use Jupyter on singularity container (see EPTA/docker_singularity):

``` 
singularity exec --bind $PWD:/home/jovyan/work/ <container_name.sif> jupyter notebook --ip="*" --no-browser
```

