import argparse
import pathlib
import re
import os
import subprocess
import glob

def initialize_run(scriptfile, 
				args, 
				tempsubmitfile, 
				singcmd,
				queuename=None, 
				ncore=1, 
				jobname="PTA analyis",
				help2=False):
	if not os.path.isfile(scriptfile):
		raise ValueError("Script file %s doesn't exist."%scriptfile)
	
	if help2:
		print("\nDisplay help of: %s\n"%scriptfile)
		subprocess.check_call('python3 %s -h'%scriptfile, shell=True)
		exit()

	# ## Make output directory
	arglist = args.split(" ")
	if "--outdir" in arglist:
		outdir = arglist[arglist.index("--outdir")+1]
	elif "-o" in arglist:
		outdir = arglist[arglist.index("-o")+1]
	else:
		raise ValueError("Output directory required in args.")

	## Set job submit file
	outsubmitfile = "%s/submit_job"%outdir

	if os.path.isdir(outdir):
		if not "--resume" in arglist:
			raise ValueError("%s already exists."%outdir)
		else:
			print("Resume run on %s"%outdir)
			Nsubmitfiles = len(glob.glob("%s/submit_job*"%outdir))
			outsubmitfile += "_res%s"%Nsubmitfiles
	else:
		subprocess.check_output('mkdir -p '+outdir, shell=True)	

	makefromtemplate(outsubmitfile, 
				tempsubmitfile, 
				"%",
				singcmd=singcmd,
				queuename=queuename,
				ncore=ncore,
				jobname=jobname,
				scriptfile=scriptfile,
				outdir=outdir,
				args=args)

	return outsubmitfile

def makefromtemplate(output,template,keyChar,**kwargs):
	"""
	Create an output file identical to the template file with the string
	between the key character KeyChar and corresponding to a keyword in kwargs
	replaced by the argument of the keyword
	@param output is the output file
	@param template is the template file
	@param keyChar is the key character surrounding keywords
	@param kwargs are keywords and corresponding arguments
	"""
	fi = open(template,'r')
	fo = open(output,'w')
	for line in fi:
		repline = line
		for kw in kwargs:
			repline = re.sub(keyChar + kw + keyChar,str(kwargs[kw]),repline)
		print(repline, end='', file=fo)
	fi.close()
	fo.close()

############################## ## MAIN PROGRAM ################################
str_help=\
'''
########################
EXAMPLES
########################

# Local sgl-psr analysis:
singularity exec ../EPTA_ENTERPRISE_220218_branch15_171336dd.sif python3 cluster_launcher/initialize_run.py --compname local -a :o examples/outdir/J1600-3053_RN30_DM100/ptmcmc_N_1e6 :p examples/toas/ :m TM,WN,RN_nb-30,RN_idx-2_nb-100 J1600-3053

# Local multi-psr analysis:
singularity exec ../EPTA_ENTERPRISE_220218_branch15_171336dd.sif python3 cluster_launcher/initialize_run.py --compname local -t multi -a :o examples/outdir/multi-psr_search/ptmcmc_N_1e6 :p examples/toas/ :m examples/psrmodels/J1600-3053_vanilla_psrmodels.json ::noisedir examples/noisefiles/ :c CRS_orf-hd

# CC-IN2P3 sgl-psr analysis:
singularity exec ../EPTA_ENTERPRISE_220218_branch15_171336dd.sif python3 cluster_launcher/initialize_run.py -a :o examples/outdir/J1600-3053_RN30_DM100/ptmcmc_N_1e6 :p examples/toas/ :m TM,WN,RN_nb-30,RN_idx-2_nb-100 J1600-3053

# CC-IN2P3 multi-psr analysis:
singularity exec ../EPTA_ENTERPRISE_220218_branch15_171336dd.sif python3 cluster_launcher/initialize_run.py -t multi -a :o examples/outdir/multi-psr_search/ptmcmc_N_1e6 :p examples/toas/ :m examples/psrmodels/J1600-3053_vanilla_psrmodels.json ::noisedir examples/noisefiles/ :c CRS_orf-hd

# Iterate on CC-IN2P3 sgl-psr analysis for several psrs written in psrnames.txt:
cat cluster_launcher/psrnames.txt | while read i; do singularity exec --bind /sps /sps/lisaf/PTA/EPTA/SImg/EPTA_ENTERPRISE_220314_branch15_070c272d.sif python3 cluster_launcher/initialize_run.py -a :o ../output/single/$i/TM_WN_RN30_DM100/ptmcmc/ :p ../data-epta/ :m TM,WN,RN,RN_idx-2_nb-100 ::Nsamples 3e6 $i --jobname RNDM_$i ; done
cat cluster_launcher/psrnames.txt | while read i; do sbatch ../output/single/$i/TM_WN_RN30_DM100/ptmcmc//submit_job ; done
'''

if __name__ == '__main__':
	currentpath = str(pathlib.Path(__file__).parent.resolve())
	scriptdir = "%s/../src"%currentpath

	parser = argparse.ArgumentParser(description="Initialize Enterprise run", epilog=str_help,
		formatter_class=argparse.RawTextHelpFormatter)

	parser.add_argument("-a", "--args", nargs="+",
		type=str, dest='args',
		help="Set arguments required in scriptfile. [required]")

	parser.add_argument("-c", "--compname",
		type=str, dest='compname', default="ccin2p3",
		help="Computer name. Currently 'ccin2p3' or 'local'. ['ccin2p3' by default]")

	parser.add_argument("-t", "--type",
		type=str, dest='type', default='sgl',
		help="Choose analysis type. 'sgl' for single-pulsar, 'multi' for multi-psr ['sgl' by default]")

	parser.add_argument("--queuename",
		type=str, dest="queuename", default="htc",
		help="Give cluster queue name (cc : 'flash', 'gpu', 'normal', 'htc' (standard single-core), 'hpc' (standard multi-core) ['htc' by default]")

	parser.add_argument("--ncore",
		type=int, dest='ncore', default=1,
		help="Number of core used [default: 1]")

	parser.add_argument("--jobname",
		type=str, dest='jobname', default="PTA_analysis",
		help="Job name [default: PTA analysis]")

	parser.add_argument("--help2",
		action="store_true", dest='help2', default=False,
		help="Display --help of the chosen script file [False by default]")

	options = parser.parse_args()

	##############################
	## Add your singularity command, the path of your submission file and the command to launch the run
	##############################

	if options.compname=="ccin2p3":
		#singcmd = "singularity exec --bind /sps /sps/lisaf/PTA/EPTA/SImg/EPTA_ENTERPRISE_220314_branch15_070c272d.sif"
		singcmd = "singularity exec --bind /sps /sps/lisaf/PTA/EPTA/SImg/EPTA_ENTERPRISE_220325_branch15_ba476399.sif"
		tempsubmitfile = "%s/submit_template_AC_slurm"%currentpath
		finalcmd = "sbatch"

	if options.compname=="dante":
		#singcmd = "singularity exec --bind /sps /sps/lisaf/PTA/EPTA/SImg/EPTA_ENTERPRISE_220314_branch15_070c272d.sif"
		singcmd = "singularity exec --bind /work /work/chalumea/EPTA_ENTERPRISE_220325_branch15_ba476399.sif"
		tempsubmitfile = "%s/submit_template_AC_slurm"%currentpath
		finalcmd = "sbatch"
	
	elif options.compname=="local":
		singcmd = "singularity exec /home/aurelienchalumeau/Documents/projets/EPTA_ENTERPRISE_220218_branch15_171336dd.sif"
		tempsubmitfile = "%s/submit_template_AC_local"%currentpath
		finalcmd = "bash"

	##############################
	##############################
	##############################
	## Define script file
	if options.type=="sgl":
		scriptfile = "%s/single_psr_analysis.py"%scriptdir
	
	elif options.type=="multi":
		scriptfile = "%s/multi_psr_analysis.py"%scriptdir
		
	## Define arguments
	args = " ".join([i.replace(":","-") for i in options.args])

	outsubmitfile = initialize_run(scriptfile=scriptfile,
									args=args,
									tempsubmitfile=tempsubmitfile,
									singcmd=singcmd,
									queuename=options.queuename,
									ncore=options.ncore,
									jobname=options.jobname,
									help2=options.help2)
	print("\n%s %s"%(finalcmd, outsubmitfile))
