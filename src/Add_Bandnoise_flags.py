import argparse
import glob
import numpy as np

def RemFlagFromtim(timfile, flagname):
	changed=False

	with open(timfile, 'r') as file :
		lines = file.readlines()

	newlines = []
	for line in lines:
		listline = line.split(" ")
		
		if "-%s"%flagname in listline:
			changed = True
			listline.pop(listline.index("-%s"%flagname)+1)
			listline.pop(listline.index("-%s"%flagname))
		
		# Check to break line
		if not listline[-1][-1:]=="\n":
			listline[-1]+="\n"
		
		newlines.append(" ".join(listline))
		
	if changed:
		print(timfile)

	with open(timfile, 'w') as file:
		file.writelines(newlines)
	
	return 

def Add_Bandnoise_flag(timfile, flagname='B', bands={"Band.1":[0,1e3],
												  "Band.2":[1e3,2e3],
												  "Band.3":[2e3,3e3],
												  "Band.4":[3e3,1e5]}):
	changed = False
	
	black_list = ['FORMAT', 'MODE', '#', 'end\n', '\n', 'TIME', 'C', 'C??', 'CC', 'CC?', 'C????']
	
	with open(timfile, 'r') as file :
		lines = file.readlines()

	newlines = []
	for line in lines:
		listline = [i for i in line.split(" ") if i != ""]
		
		# Check that is a good line and that flag doesn'exist
		if listline[0] not in black_list and not '-%s'%flagname in listline:
			changed = True
			# Find band
			freq = float(listline[1])
			for key, val in bands.items():
				if (freq >= val[0]) and (freq < val[1]):
					band = key
			listline[-1] = listline[-1].replace("\n","")
			listline.append('-%s %s\n'%(flagname,band))
			newlines.append(" ".join(listline))
		else:
			newlines.append(line)
	
	if changed:
		print(timfile)

	with open(timfile, 'w') as file:
		file.writelines(newlines)

	return 

# singularity exec ../EPTA_ENTERPRISE_220308_93621da1.sif python3 src/Add_Bandnoise_flags.py -d ../data-epta/ --nopsrs J1751-2857

def get_parser():
	parser = argparse.ArgumentParser(description="Temporary script used to add bandnoise flag in timfiles",formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	# General
	parser.add_argument('-d','--datadir', 
		type=str, default=None,
		help="Data directory organized as datadir/J*/tims/timfiles [None by default]")

	parser.add_argument('-t','--timfiles', nargs="+",
		type=str, default=None,
		help="Timfiles, used if no datadir given. [None by default]")
	
	parser.add_argument('--flagname', 
		type=str, default="bandnoise", 
		help="Flag name for Band noise [bandnoise by default]")
	
	parser.add_argument('--remflag', 
		action="store_true", default=False, 
		help="Remove given flag name. [False by default]")

	parser.add_argument('--nopsrs', 
		type=str, default=None, 
		help="Avoid given pulsars. [None by default]")
	return parser


if __name__ == "__main__":
	parser = get_parser()
	options, args = parser.parse_known_args()
	datadir = options.datadir
	timfiles = options.timfiles
	flagname = options.flagname
	remflag = options.remflag
	nopsrs = options.nopsrs
	
	nopsrs = nopsrs.split(",") if nopsrs else []
		
	if datadir:
		timfiles = np.sort(glob.glob(datadir+"/J*/tims/*"))

	if not remflag:
		print("\nAdd Band noise flags to:")
		for timfile in timfiles:
			for nopsr in nopsrs:
				if nopsr in timfile:
					break
			else:
				Add_Bandnoise_flag(timfile, flagname)
	else:
		print("\nRemove flage %s in:"%flagname)
		for timfile in timfiles:
			RemFlagFromtim(timfile, flagname)
	print("\n")
