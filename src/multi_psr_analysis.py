import argparse
import os
import sys
import json
import numpy as np
import libstempo as LT
from enterprise.pulsar import Pulsar
from build_enterprise_signals import MakeMultiPsrModel
from sampler_output import sampler_output

def analyse_multi_psr(pathdirdata,
				  outdir,
				  psrmodelfile,
				  commonmodel,
				  LTfit=10,
				  ephem=None,
				  noisedir=None,
				  fixpars=None,
				  sampler="ptmcmc",
				  Nsamples=1e6,
				  log_weights=None,
				  dlogz=0.01,
				  dynsample='rwalk',
				  pfrac=None,
				  Nthreads=None,
				  empdist_dir=None,
				  empdist_kde=False,
				  resume=False):
	###### Check Outdir
	if not os.path.isdir(outdir):
		raise ValueError("%s doesn't exist."%outdir)

	###### Get psrnames and their noise models from .json
	psrnames, psrmodels = [], []
	for i, p in enumerate(psrmodelfile):
		with open(p, "r") as f:
			psrmodel = json.load(f)
			psrmodels.append(psrmodel)
		psrnames.append(list(np.sort(list(psrmodel.keys()))))

		## Check that all psr lists are the same
		if i>0:
			if not psrnames[0]==psrnames[i]:
				raise ValueError("Pulsar lists should be the same for every psrmodelfiles.")
	
	psrnames = psrnames[0]
	
	###### Create enterprise pulsar objects for each psr
	psrs = []
	for p, psrname in enumerate(psrnames):
		print("Add %s..."%psrname)
		###### Define the pulsars files
		basePath = pathdirdata+'/'+psrname+'/'+psrname
		print("\nParfile & timfile :")
		print(basePath+'.par')
		print(basePath+'_all.tim\n')

		###### Load the pulsars with libstempo
		# print("\nRead ToAs with libstempo.")
		ltpsr = LT.tempopulsar(basePath+'.par', basePath+'_all.tim', ephem=ephem)
		
		###### Fit TM (or not)
		for i in range(LTfit):
			if i==0: print("Fit %s times for the timing model."%LTfit)
			ltpsr.fit()

		###### Create enterprise pulsar objects
		# print("\nMake enterprise pulsar object.")
		## Save T2 pulsar object if fit for TM during MCMC
		drop_t2pulsar=True
		for p in psrmodels:
			if "TM_fit" in p[psrname]:
				drop_t2pulsar=False
		psrs.append(Pulsar(ltpsr, ephem=ephem, drop_t2pulsar=drop_t2pulsar))

	###### Make models
	print("\nBuild enterprise signals and model.")
	commonmodel_list = commonmodel.split("/")
	model = []
	if len(commonmodel_list)>1 and len(psrmodelfile)>1:
		if len(commonmodel_list) != len(psrmodelfile):
			raise ValueError("Common models must have same length than psr model files if both are multiples.")
		
		print("\nHyperModel run with common models: %s."%(" + ".join(commonmodel_list)))
		print("And single-psr models: %s."%" + ".join([os.path.basename(i) for i in psrmodelfile]))
		for i in range(len(psrmodels)):
			print("\n###### Model %s:"%i)
			model.append(MakeMultiPsrModel(psrs, psrmodels[i], commonmodel_list[i]))

	elif len(commonmodel_list)>1:
		print("\nHyperModel run with common models: %s."%(" + ".join(commonmodel_list)))
		print("And for all, the single-psr model: %s"%os.path.basename(psrmodelfile[0]))
		for i in range(len(commonmodel_list)):
			print("\n###### Model %s:"%i)
			model.append(MakeMultiPsrModel(psrs, psrmodels[0], commonmodel_list[i]))
	
	elif len(psrmodels)>1:
		print("\nHyperModel run with single-psr models: %s."%" + ".join([os.path.basename(i) for i in psrmodelfile]))
		print("And for all, the common model: %s"%commonmodel_list[0])
		
		for i in range(len(psrmodels)):
			print("\n###### Model %s:"%i)
			model.append(MakeMultiPsrModel(psrs, psrmodels[i], commonmodel_list[0]))
	else:
		model = MakeMultiPsrModel(psrs, psrmodels[0], commonmodel_list[0])

	###### Build PTA object, set output directory and sample
	sampler_output(model=model, 
				sampler=sampler, 
				outdir=outdir, 
				noisedir=noisedir, 
				fixpars=fixpars,
				Nsamples=Nsamples,
				log_weights=log_weights,
				dlogz=dlogz, 
				dynsample=dynsample, 
				pfrac=pfrac, 
				Nthreads=Nthreads,
				empdist_dir=empdist_dir, 
				empdist_kde=empdist_kde, 
				resume=resume)

#####################################
## MAIN
# TODO: 
#   - Add other samplers (Bilby, ...)
#   - Allow PT with PTMCMC.
#	- Add kde Jumps
#####################################
str_help=\
'''
########################
How to write noise models:
########################
Add signals separated with ','
For each signal, define arguments separated with '_'.
To use HyperModel to compare 2 or more models, either:
	- Give several psrmodelfile. If you do that, all psrmodelfiles should have the same pulsar list.
	- Separate commonmodel with '/'
	- Do both. If you do that: len(psrmodelfile) should be equal to len(commonmodel)
See list of signals and arguments below:

####### Gaussian process Common Red Signal: CRS
psd: Choose the type of psd. pl for simple powerlaw, bpl for broken-power law, fs for free-spectrum.
nb: Number of freq. bins (default: 30).
orf: Overlap reduction function. None for CURN (default: None).
log: Logspaced bins.
- `dp`: Dropout (only available for powerlaw psd), (default=None).
- `dp_psr`: Give pulsar name and include `dp` to add a dropout parameter only related with the selected pulsar.
# Ex: CRS_nb-30_log, RN_nb-30_psd-fs

####### BayesEphem SSE error model: BFM
Includes masses of Jup,Sat,Ura,Nep, orbital elements of Jup,Sat and a rotation (drift) rate of the frame
# Ex: BFM

####### EphemGP SSE error model: EGP
Includes orbital elements of chosen bodies between Jup,Sat,Ura,Nep,Earth-Moon barycenter,Mars (default: 'Jup,Sat')
bod: Selected bodies
# Ex: EGP_bod-Jup-Sat

####### Available ORFs:
None or 'crn': CURN
'dipole': Dipole
'monopole': Monopole
'hd': Hellings-Downs.

'bin_orf': Agnostic binned spatial correlation function. Bin edges are placed at edges and across angular separation space.
'chebyshev_orf': Chebyshev polynomial decomposition of the spatial correlation function.
'spline_orf': Agnostic spline-interpolated spatial correlation function. Spline knots are placed at edges, zeros, and minimum of H&D curve.
'legendre_orf': Legendre polynomial spatial correlation function. Assumes process normalization such that autocorrelation signature is 1.

'zero_diag_hd': Off-diagonal Hellings & Downs spatial correlation function. To be used in a "split likelihood" model with an additional common uncorrelated red process. The latter is necessary to regularize the overall Phi covariance matrix.
'zero_diag_bin_orf': Agnostic binned spatial correlation function. To be used in a "split likelihood" model with an additional common uncorrelated red process. The latter is necessary to regularize the overall Phi covariance matrix.
'zero_diag_legendre_orf': Legendre polynomial spatial correlation function. To be used in a "split likelihood" model with an additional common uncorrelated red process. The latter is necessary to regularize the overall Phi covariance matrix.

'param_hd': Pre-factor parametrized Hellings & Downs spatial correlation function (Taylor 2020).
'gw_monopole': GW-Monopole (Laal 2020).
'gw_dipole': GW-Dipole (Laal 2020).
'freq_hd': Frequency-dependent Hellings & Downs spatial correlation function. Implemented as a model that only enforces H&D inter-pulsar correlations after a certain number of frequencies in the spectrum. The first set of frequencies are uncorrelated.
'st': Scalar tensor correlations as induced by the breathing polarization mode of gravity (Laal 2020).
'gt': General Transverse (GT) Correlations. This ORF is used to detect the relative significance of all possible correlation patterns induced by the most general family of transverse gravitational waves (Laal 2020).


########################
EXAMPLES
########################

### Common red signal analysis with Hellings-Downs ORF, and TM,WN fixed,RN30,DM100 model for all pulsars sampled with PTMCMC with 1e6 iterations:
singularity exec ../EPTA_ENTERPRISE_220218_branch15_171336dd.sif python3 src/multi_psr_analysis.py -o playground/examples/outdir/ -p playground/examples/toas/ -m playground/examples/psrmodels/J1600-3053_vanilla_psrmodels.json --noisedir playground/examples/noisefiles/ -c CRS_orf-hd
'''

if __name__ == '__main__':
	scriptPath = os.path.realpath(__file__)

	parser = argparse.ArgumentParser(description="Multi-psr analysis run", epilog=str_help,
		formatter_class=argparse.RawTextHelpFormatter)

	parser.add_argument("-p", "--pathdirdata",
		type=str, dest='pathdirdata', required=True,
		help="Path to the directory containing the data [required]")

	parser.add_argument("-o", "--outdir",
		type=str, dest='outdir', required=True,
		help="Path to the output directory [required]")

	parser.add_argument("-m", "--psrmodelfile",
		type=str, dest='psrmodelfile', required=True, nargs='+',
		help="Json file containing single psr noise models. See 'single_psr_analysis.py' to see how to set models. Use several for HyperModel: All sub-psrmodels are linked either with (1) the same commonmodel if len(commonmodel)=1, or (2) with the same index commonmodel (len(commonmodel)=len(psrmodelfile) required if so) [required]")

	parser.add_argument("-c", "--commonmodel",
		type=str, dest='commonmodel', required=True,
		help="String used to set the multi-psr noise components. Only PSR noise components if None. Separate models with '/' for HyperModel: All sub-commonmodels are linked either with (1) the same psrmodelfile if len(psrmodelfile)=1, or (2) with the same index psrmodel (len(psrmodelfile)=len(commonmodel) required if so) [None by default]")	

	parser.add_argument("--LTfit",
		type=int, dest='LTfit', default=10,
		help="Number of fits of the pulsar model, with Libstempo. 0 for no fitting [10 by default]")

	parser.add_argument("--ephem",
		type=str, dest='ephem', default=None,
		help="Solar-system ephemeris [None by default]")

	parser.add_argument("--noisedir",
		type=str, dest='noisedir', default=None,
		help="Directory with Json noisefiles to fix WN params [None by default]")

	parser.add_argument("--fixpars",
		type=str, dest='fixpars', default=None,
		help="Give values to set as Constant, Ex: --fixpars efac:1,log10_equad:-10 [None by default]")

	parser.add_argument("--sampler",
		type=str, dest='sampler', default="ptmcmc",
		help="Sampler: 'ptmcmc', 'dynesty' or 'mc3'. For dynesty : dynesty, Mdynesty, ddynesty or Mddynesty [ptmcmc by default]")

	parser.add_argument("--Nsamples",
		type=float, dest='Nsamples', default=1e6,
		help="Number of samples [default: 1e6]")

	parser.add_argument("--log_weights",
		type=str, dest='log_weights', default=None, 
		help="List of values added to log_likelihood of each nmodel. Ex : 2,3 [None by default]")

	parser.add_argument("--dlogz",
		type=float, dest='dlogz', default=0.01,
		help="Log evidence variation used to end the run for dynesty [0.01 by default]")

	parser.add_argument("--dynsample",
		type=str, dest='dynsample', default="rwalk",
		help="Sampling method for dynesty (rwalk, slice, rslice, hslice, unif, auto) [rwalk by default]")

	parser.add_argument("--pfrac",
		type=float, dest='pfrac', default=None,
		help="Posterior priority fraction for dynamic dynesty [None by default]")

	parser.add_argument("--Nthreads",
		type=int, dest="Nthreads", default=None,
		help="Define number of threads used for Mdynesty [1 by default]")

	parser.add_argument("--empdist_dir",
		type=str, dest='empdist_dir', default=None,
		help="Path to the empirical distribution pickle [None by default]")

	parser.add_argument("--empdist_kde",
		action="store_true", dest='empdist_kde', default=False,
		help="Activate HyperKDE method (TODO) [None by default]")

	parser.add_argument("--resume",
		action="store_true", dest="resume", default=False,
		help="Restart the sampling from the chain of the previous run (chain1.txt). [off by default]")

	options, args = parser.parse_known_args()

	if len(args) != 0:
		print(args)
		parser.error("No argument to use (see --help)!")

	print("Python command: %s"%(" ".join(sys.argv)))	

	analyse_multi_psr(pathdirdata=options.pathdirdata,
				  outdir=options.outdir,
				  psrmodelfile=options.psrmodelfile,
				  commonmodel=options.commonmodel,
				  LTfit=options.LTfit,
				  ephem=options.ephem,
				  noisedir=options.noisedir,
				  fixpars=options.fixpars,
				  sampler=options.sampler,
				  Nsamples=options.Nsamples,
				  log_weights=options.log_weights,
				  dlogz=options.dlogz,
				  dynsample=options.dynsample,
				  pfrac=options.pfrac,
				  Nthreads=options.Nthreads,
				  empdist_dir=options.empdist_dir,
				  empdist_kde=options.empdist_kde,
				  resume=options.resume)
