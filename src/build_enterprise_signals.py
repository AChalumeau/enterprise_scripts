import numpy as np
from collections import OrderedDict
from enterprise.signals import gp_signals, parameter
from enterprise.signals import deterministic_signals
from enterprise_extensions.timing import timing_block
#from enterprise_extensions.blocks import white_noise_block, red_noise_block, dm_noise_block, chromatic_noise_block, common_red_noise_block
from provisory_blocks import white_noise_block, red_noise_block, dm_noise_block, chromatic_noise_block, common_red_noise_block
from provisory_parameter import Discrete
from enterprise_extensions import chromatic as chrom
from enterprise_extensions import model_utils

# import sys
# sys.path.append("/home/aurelienchalumeau/Documents/projets/Ephemvspta/EphemGP/")
# from EphemGP_Model import ephemGP

def get_modes(nbins, Tspan, log=False):
	"""
	Set frequency modes (bins) for the Gaussian processes.
	"""
	fmin = 1 / Tspan
	fmax = nbins / Tspan
	if log:
		modes = np.logspace(np.log10(fmin), np.log10(fmax), nbins)
	else:
		modes = np.linspace(fmin, fmax, nbins)
	return modes

def get_signal_arg(sigargs, argname, booltype=False):
	"""
	Read signal string arguments. Either boolean or with a value given as '<signam>-<sigval>''
	"""
	if booltype:
		if any(sp[:len(argname)]==argname for sp in sigargs):
			return True
		else:
			return False
	else:
		if any(sp[:len(argname)]==argname for sp in sigargs):
			sp_idx = [ii for ii, sp in enumerate(sigargs) if sp[:len(argname)]==argname][0]
			vals = sigargs[sp_idx].split("-")[1:]
			return vals
		else:
			return None

def MakeSglPsrSignal(psr, model):
	"""
	Build single-pulsar Enterprise signal from a string defining the noise model.
	psr: Enterprise pulsar object
	model: String defining the model. Enter each signal separated with ',', with detail arguments separated with '_'. Ex: TM,WN,RN_nb-30,RN_idx-2_nb-100 defines a noise model composed of marginalized TM, white noise, achromatic red noise with 30 frequency bins and DM variations with 100 freq. bins.
	"""
	rM = "\n### Noise Model %s: %s"%(psr.name, model)
	signals = model.split(",")
	
	####### Add single-psr signals
	s_list = []
	for sig in signals:
		siglist = sig.split("_")
		signame, sigargs = siglist[0], siglist[1:]

		####### Timing Model
		if signame=="TM":
			sig = 8 if get_signal_arg(sigargs, "sig") is None else float(get_signal_arg(sigargs, "sig")[0])
			tmpars = get_signal_arg(sigargs, "pars")
			fit = get_signal_arg(sigargs, "fit", booltype=True)
			
			rM += "\n- timing model"
			if fit:
				psr.tmparams_orig = OrderedDict.fromkeys(psr.t2pulsar.pars())
				for key in psr.tmparams_orig:
					psr.tmparams_orig[key] = (psr.t2pulsar[key].val,  psr.t2pulsar[key].err*sig)
				
				if tmpars:
					tmparam_list = tmpars
				else:
					tmparam_list = list(psr.tmparams_orig)

				rM += ", Fit for %s"%",".join(tmparam_list)
				s_list.append(timing_block(tmparam_list=tmparam_list))

			else:
				rM += ", marginalized"
				s_list.append(gp_signals.TimingModel(use_svd=True, normed=True, coefficients=False))
			
		####### White noise
		if signame=="WN": # Warning: Always include White Noise !
			fix = get_signal_arg(sigargs, "fix", booltype=True)
			select = "backend" if get_signal_arg(sigargs, "select") is None else get_signal_arg(sigargs, "select")[0]
			vary = True if not fix else False

			
			rM += "\n- White-noise, varied" if vary else "\n- White-noise, fixed"
			s_list.append(white_noise_block(vary=vary, tnequad=True, select=select))
			#s_list.append(white_noise_block(vary=vary))
			
		####### Gaussian process red noise: RN
		if signame=="RN":
			nbins = 30 if get_signal_arg(sigargs, "nb") is None else int(get_signal_arg(sigargs, "nb")[0])
			idx = get_signal_arg(sigargs, "idx")
			psd = "pl" if get_signal_arg(sigargs, "psd") is None else get_signal_arg(sigargs, "psd")[0]
			log = get_signal_arg(sigargs, "log", booltype=True)
			dp = get_signal_arg(sigargs, "dp", booltype=True)
			dpbin = get_signal_arg(sigargs, "bin", booltype=True)
			fn = get_signal_arg(sigargs, "fn")
			fv = get_signal_arg(sigargs, "fv")

			if psd=="pl" : psd = "powerlaw" 
			if psd=="bpl" : psd = "broken_powerlaw"
			if psd=="fs" : psd = "spectrum"

			distribtype = "log" if log else "linear"

			dp_str="with a dropout factor " if dp else ""

			if fn and fv:
				flagname = fn[0]
				flagvals = fv
				for flagval in flagvals:
					seltoas = psr.toas[np.where(psr.flags[flagname]==flagval)]
					seltspan = seltoas.max() - seltoas.min()
					modes = get_modes(nbins, seltspan, log=log)

					if not idx:
						rM += "\n- Selected achromatic red-noise %s %sfor %s: %s with %s frequency bins using %s distribution."%(psd, dp_str, flagname, flagval, str(len(modes)), distribtype)
						s_list.append(red_noise_block(psd=psd, modes=modes, name="srn_%s_%s"%(flagname,flagval), components=nbins, delta_val=0, dropout=dp, dropbin=dpbin, select={flagname:flagval}))
						#s_list.append(red_noise_block(psd=psd, name="srn_%s_%s"%(flagname,flagval), components=nbins, Tspan=seltspan, delta_val=0, dropout=dp, dropbin=dpbin, select={flagname:flagval}))
						#rM += "\n- Selected achromatic red-noise %s %sfor %s: %s with frequency bins from %s to %s using %s distribution."%(psd, dp_str, flagname, flagval, binl, binh, distribtype)
						
					elif idx[0]=="2":
						rM += "\n- Selected DM variations %s %sfor %s: %s with %s frequency bins using %s distribution."%(psd, flagname, dp_str, flagval, str(len(modes)), distribtype)
						s_list.append(dm_noise_block(psd=psd, modes=modes, name="dm_srn_%s_%s"%(flagname,flagval), components=nbins, delta_val=0, dropout=dp, dropbin=dpbin, select={flagname:flagval}))
						#s_list.append(dm_noise_block(psd=psd, name="dm_srn_%s_%s"%(flagname,flagval), components=nbins, Tspan=seltspan, delta_val=0, dropout=dp, dropbin=dpbin, select={flagname:flagval}))
						#rM += "\n- Selected DM variations %s %sfor %s: %s with frequency bins using %s distribution."%(psd, flagname, dp_str, flagval, binl, binh, distribtype)
					elif idx[0]=="free":
						rM += "\n- Selected Free-chrom. noise %s %sfor %s: %s with %s frequency bins using %s distribution and chromatic index: U(0.10)."%(psd, dp_str, flagname, flagval, str(len(modes)), distribtype)
						idx = parameter.Uniform(0, 10)
						s_list.append(chromatic_noise_block(gp_kernel='diag', psd=psd, modes=modes, name="fc_srn_%s_%s"%(flagname,flagval), idx=idx, components=nbins, delta_val=0., dropout=dp, dropbin=dpbin, select={flagname:flagval}))
						#s_list.append(chromatic_noise_block(gp_kernel='diag', psd=psd, name="fc_srn_%s_%s"%(flagname,flagval), idx=idx, components=nbins, Tspan=seltspan, delta_val=0., dropout=dp, dropbin=dpbin, select={flagname:flagval}))
						#rM += "\n- Selected Free-chrom. noise %s %sfor %s: %s with frequency bins from %s to %s using %s distribution and chromatic index: U(0.10)."%(psd, dp_str, flagname, flagval, binl, binh, distribtype)
					else:
						rM += "\n- Selected chrom. noise %s %sfor %s: %s with %s frequency bins using %s distribution and chromatic index: %s."%(psd, dp_str, flagname, flagval, str(len(modes)), distribtype, float(idx[0]))
						s_list.append(chromatic_noise_block(gp_kernel='diag', psd=psd, modes=modes, name="cn-%s_srn_%s_%s"%(flagname,flagval)%float(idx[0]), idx=float(idx[0]), components=nbins, delta_val=0., dropout=dp, dropbin=dpbin, select={flagname:flagval}))
						#s_list.append(chromatic_noise_block(gp_kernel='diag', psd=psd, name="cn-%s_srn_%s_%s"%(flagname,flagval)%float(idx[0]), idx=float(idx[0]), components=nbins, Tspan=seltspan, delta_val=0., dropout=dp, dropbin=dpbin, select={flagname:flagval}))
						#rM += "\n- Selected chrom. noise %s %sfor %s: %s with frequency bins from %s to %s using %s distribution and chromatic index: %s."%(psd, dp_str, flagname, flagval, binl, binh, distribtype, float(idx[0]))
			else:
				Tspan = psr.toas.max() - psr.toas.min()
				modes = get_modes(nbins, Tspan, log=log)
				if not idx:
					rM += "\n- Achromatic red-noise %s %swith %s frequency bins using %s distribution."%(psd, dp_str, str(len(modes)), distribtype)
					s_list.append(red_noise_block(psd=psd, modes=modes, name="rn", components=nbins, delta_val=0, dropout=dp, dropbin=dpbin))
					# rM += "\n- Achromatic red-noise %s %swith frequency bins from %s to %s using %s distribution."%(psd, dp_str, binl, binh, distribtype)
					# s_list.append(red_noise_block(psd=psd, name="rn", components=nbins, delta_val=0, dropout=dp))
				elif idx[0]=="2":
					rM += "\n- DM variations %s %swith %s frequency bins using %s distribution."%(psd, dp_str, str(len(modes)), distribtype)
					s_list.append(dm_noise_block(psd=psd, modes=modes, name="dm", components=nbins, delta_val=0, dropout=dp, dropbin=dpbin))
					# rM += "\n- DM variations %s %swith frequency bins from %s to %s using %s distribution."%(psd, dp_str, binl, binh, distribtype)
					# s_list.append(dm_noise_block(psd=psd, name="dm", components=nbins, delta_val=0, dropout=dp))
				elif idx[0]=="free":
					rM += "\n- Free-chrom. noise %s %swith %s frequency bins using %s distribution and chromatic index: U(0.10)."%(psd, dp_str, str(len(modes)), distribtype)
					idx = parameter.Uniform(0, 10)
					s_list.append(chromatic_noise_block(gp_kernel='diag', psd=psd, modes=modes, name="fc", idx=idx, components=nbins, delta_val=0., dropout=dp, dropbin=dpbin))
					#s_list.append(chromatic_noise_block(gp_kernel='diag', psd=psd, name="fc", idx=idx, components=nbins, delta_val=0., dropout=dp, dropbin=dpbin))
					#rM += "\n- Free-chrom. noise %s %swith frequency bins from %s to %s using %s distribution and chromatic index: U(0.10)."%(psd, dp_str, binl, binh, distribtype)
				else:
					rM += "\n- Chrom. noise %s %swith %s frequency bins using %s distribution and chromatic index: %s."%(psd, dp_str, str(len(modes)), distribtype, float(idx[0]))
					s_list.append(chromatic_noise_block(gp_kernel='diag', psd=psd, modes=modes, name="cn-%s"%float(idx[0]), idx=float(idx[0]), components=nbins, delta_val=0., dropout=dp, dropbin=dpbin))
					#s_list.append(chromatic_noise_block(gp_kernel='diag', psd=psd, name="cn-%s"%float(idx[0]), idx=float(idx[0]), components=nbins, delta_val=0., dropout=dp, dropbin=dpbin))
					#rM += "\n- Chrom. noise %s %swith frequency bins from %s to %s using %s distribution and chromatic index: %s."%(psd, dp_str, binl, binh, distribtype, float(idx[0]))
					

		####### Exponential dip
		if signame=="E":
			tmin = float(get_signal_arg(sigargs, "ti")[0])
			tmax = float(get_signal_arg(sigargs, "tf")[0])
			idx = 2 if get_signal_arg(sigargs, "idx") is None else get_signal_arg(sigargs, "idx")[0]
			if idx[0]=="free":
				rM += "\n- Exponential dip from %s to %s with chromatic index: U(0,10)."%(tmin, tmax)
				idx = parameter.Uniform(0, 10)
				s_list.append(chrom.dm_exponential_dip(tmin=tmin, tmax=tmax, idx=idx, sign='negative', name='free-expd_%s_%s'%(int(tmin),int(tmax))))
			else:
				rM += "\n- Exponential dip from %s to %s with chromatic index: %s."%(tmin, tmax, idx)
				s_list.append(chrom.dm_exponential_dip(tmin=tmin, tmax=tmax, idx=float(idx[0]), sign='negative', name='expd-%s_%s_%s'%(idx, int(tmin),int(tmax))))

		####### Annual chromatic noise
		if signame=="Y":
			idx = 2 if get_signal_arg(sigargs, "idx") is None else get_signal_arg(sigargs, "idx")[0]
			if idx[0]=="free":
				rM += "\n- Yearly chrom. noise with chromatic index: U(0,10)."
				idx = parameter.Uniform(0, 10)
				s_list.append(chrom.dm_annual_signal(idx=idx, name='yearly'))
			else:
				rM += "\n- Yearly chrom. noise with chromatic index: %s."%(idx)
				s_list.append(chrom.dm_annual_signal(idx=float(idx[0]), name='yearly-%s'%idx))


	s = s_list[0]
	for sl in s_list[1:]:
		s+=sl
		
	print(rM)

	return s

def MakeMultiPsrModel(psrs, psrmodels, commonmodel):
	"""
	Build multi-pulsar Enterprise signal from (1) a dictionnary for each single-psr noise model and (2) a string defining the model for the common signals..
	psrs: List of Enterprise pulsar object
	psrmodels: Dictionnary for each single-psr noise model. Ex: {"J1909-3744":"TM,WN,RN_nb-30,RN_idx-2_nb-100"}
	commonmodel: String defining the common red signals to add in the model. Ex: "CRS_orf-hd"
	"""
	rM = "\n### Common Noise/Signal Model: %s"%commonmodel
	signals = commonmodel.split(",")
	Tspan = model_utils.get_tspan(psrs)

	####### Add common (multi-pulsar) signals
	s_list = []
	for sig in signals:
		siglist = sig.split("_")
		signame, sigargs = siglist[0], siglist[1:]

		####### Common-Red Signal: Gaussian process signal with Kernel set from PSD
		if signame=="CRS":
			psd = "pl" if get_signal_arg(sigargs, "psd") is None else get_signal_arg(sigargs, "psd")[0]
			prior = 'log-uniform'
			nbins = 30 if get_signal_arg(sigargs, "nb") is None else int(get_signal_arg(sigargs, "nb")[0])
			orf = get_signal_arg(sigargs, "orf")
			log = get_signal_arg(sigargs, "log", booltype=True)
			dp = get_signal_arg(sigargs, "dp", booltype=True)
			dp_psr = get_signal_arg(sigargs, "dp_psr")
			idx = get_signal_arg(sigargs, "idx")

			if psd=="pl" : psd = "powerlaw"
			if psd=="bpl" : psd = "broken_powerlaw"
			if psd=="fs" : psd = "spectrum"

			if orf is not None : orf = ",".join(orf)
			
			distribtype = "log" if log else "linear"

			dp_str="with a dropout factor " if dp else ""
			
			modes = get_modes(nbins, Tspan, log=log)

			rM += "\n- Common red noise: %s psd, orf: %s %s, %s components using %s distribution, amp. prior: %s, idx: %s."%(psd, str(orf), dp_str, nbins, distribtype, prior, idx)
			if dp_psr: rM += " Dropout psr : %s."%dp_psr
			if idx is not None:
				if idx[0]=="free": 
					idx = parameter.Uniform(0, 7)('gw_%s_idx'%orf)
				else:
					idx=float(idx[0])

			s_list.append(common_red_noise_block(psd=psd, prior=prior, Tspan=Tspan, components=nbins, tnfreq=False, log10_A_val=None, gamma_val=None, idx=idx,
					                           delta_val=0., orf=orf, orf_ifreq=0, leg_lmax=5, name='gw_%s'%orf, modes=modes, dropout=dp, dropout_psr=dp_psr))

		####### BayesEphem
		if signame=="BFM":
			be_model = 'setIII_1980'

			rM += "\n- Ephemeris model : BayesEphem, %s"%(be_model)
			s_list.append(deterministic_signals.PhysicalEphemerisSignal(use_epoch_toas=True, model=be_model, sat_orb_elements=True))

		####### EphemGP
		if signame=="EGP":
			egp_bodies = ["Jup", "Sat"] if get_signal_arg(sigargs, "bod") is None else get_signal_arg(sigargs, "bod")
			egp_version = "INPOP21a" if get_signal_arg(sigargs, "ver") is None else get_signal_arg(sigargs, "ver")[0]
			global_N = 1e12 if get_signal_arg(sigargs, "N") is None else float(get_signal_arg(sigargs, "N")[0])
			margin = get_signal_arg(sigargs, "marg", booltype=True)

			rM += "\n- Ephemeris model : EphemGP, bodies: %s, version: %s, global_N: %.2e, margin: %s"%(egp_bodies, egp_version, global_N, margin)
			s_list.append(ephemGP(bodies=egp_bodies, inpop_version=egp_version, global_N=global_N, margin=margin))

	####### Assemble common signals together (dummy)
	s_com = None
	if s_list:
		s_com = s_list[0]
		if len(s_list)>1:
			for sl in s_list[1:]:
				s_com+=sl

	####### Add single-psr signals and build full model
	model = []
	for psr in psrs:
		psrmodel = psrmodels[psr.name]
		
		s_sgl = MakeSglPsrSignal(psr, psrmodel)
		if s_com is not None:
			s = s_sgl + s_com
		else:
			s = s_sgl
		model.append(s(psr))

	print(rM)

	return model
