import os,re
import pickle
import json
import numpy as np
from scipy import stats
from optparse import OptionParser
from datetime import datetime
from dynesty import utils as dyfunc
# Example : singularity exec --bind /media ../../EPTA_ENTERPRISE_210218_71c8e4a5.sif python3 MakeNoiseFiles.py --chaindir ptmcmc1e6_MRN30DM30_J1909-3744 --outdir ../noisefiles/ --sampler dynesty

############################## ## FUNCTION ################################

def MakeNoiseFiles(chaindir,\
				  outfile,\
				  method="MAP",\
				  sampler="ptmcmc",\
				  burn=0.3,
				  Npts=1000):
	print("Work on %s..."%chaindir)
	# Read data
	if sampler=="ptmcmc":
		chain = np.loadtxt(chaindir+"/chain_1.txt")
		#chain = np.load(chaindir+"/chain_1.npy")
		chain = chain[int(len(chain)*burn):, :-4]
		Prob = chain[int(len(chain)*burn):, -4]
		LH = chain[int(len(chain)*burn):, -3]
		pars = np.loadtxt(chaindir+"/pars.txt", dtype=str)

	elif sampler=="dynesty":
		chfile = chaindir+"/DynRes.pkl"
		with open(chfile,'br') as f:
			pkl = pickle.load(f)

		chsamp = pkl["samples"]
		chw = np.exp(pkl["logwt"] - pkl["logz"][-1])
		chain = dyfunc.resample_equal(chsamp, chw)
		LH = pkl['logl']
		pars = np.loadtxt(chaindir+"/pars.txt", dtype=str)

	x = {}
	# Dummy
	if method=="median":
		for ct, par in enumerate(pars):
			x[par] = np.median(chain[:, ct])
	
	if  method=="maxLH":
		for ct, par in enumerate(pars):
			x[par] = chain[np.argmax(LH),ct]	

	if method=="MAP":
		# Compute KDE from chain
		kde = stats.gaussian_kde(chain.T)

		# Compute probability of randomly selected chain points
		#np.random.seed(42)
		#Npts = 1000
		idxs = np.random.choice(range(len(chain)), Npts, replace=False)
		P = []
		for i in range(Npts):
			print("%s on %s"%(str(i), str(Npts)), end="\r")
			P.append(kde(chain[idxs[i]]))
		#P = [kde(chain[idxs[i]]) for i in range(Npts)]

		# Select max prob index
		sel_idx = np.argmax(P)

		# Set values
		for ct, par in enumerate(pars):
			x[par] = chain[sel_idx, ct]

	print("Save noise results in %s"%outfile)
	with open(outfile, 'w') as f:
		json.dump(x, f, sort_keys=True, indent=4, separators=(',', ': '))
	
############################## ## MAIN PROGRAM ################################

if __name__ == '__main__':
	parser = OptionParser(usage="usage: %prog  [options]",
						  version="A. Chalumeau - 05/04/2021 - $Id$")

	parser.add_option("-c", "--chaindir",
		type="string", dest='chaindir', default=None,
		help="Path to the chain directory [Required]")

	parser.add_option("-o", "--outfile",
		type="string", dest='outfile', default=None,
		help="Out noisefile nams. [Required]")

	parser.add_option("-m", "--method",
		type="string", dest='method', default='MAP',
		help="Method to obtain values. MAP, maxLH, median. [MAP by default]")

	parser.add_option("-s", "--sampler",
		type="string", dest='sampler', default='ptmcmc',
		help="Samplers : ptmcmc & dynesty. [ptmcmc by default]")

	parser.add_option("-b", "--burn",
		type="float", dest='burn', default=.3,
		help="Burnin. [0.30 by default]")

	parser.add_option("-N", "--Npts",
		type="int", dest='Npts', default=1000,
		help="Number of points to build kde for MAP method. [1000 by default]")

	(options, args) = parser.parse_args()

	MakeNoiseFiles(chaindir=options.chaindir,\
				  outfile=options.outfile,\
				  method=options.method,\
				  sampler=options.sampler,\
				  burn=options.burn,
				  Npts=options.Npts)




