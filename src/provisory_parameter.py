import numpy as np
from enterprise.signals.parameter import Parameter, Function, _argrepr

def DisPrior(value, pmin, pmax):
    """Prior function for Discrete parameters.
    Accepts vector value and boundaries."""

    return 1 # Should be wrong !


def DisSampler(pmin, pmax, size=None):
    """Sampling function for Discrete parameters."""

    return np.random.randint(pmin, pmax, size=size)


def Discrete(pmin, pmax, size=None):
    """Class factory for Discrete parameters
    :param pmin: minimum of discrete uniform range
    :param pmax: maximum of discrete uniform range
    :param size: length for vector parameter
    :return:     ``Uniform`` parameter class
    """

    class Discrete(Parameter):
        _size = size
        _prior = Function(DisPrior, pmin=pmin, pmax=pmax)
        _sampler = staticmethod(DisSampler)
        _typename = _argrepr("Integer", pmin=pmin, pmax=pmax)

    return Discrete