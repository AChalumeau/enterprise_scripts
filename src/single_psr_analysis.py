import argparse
import os
import sys
import numpy as np
import libstempo as LT
from enterprise.pulsar import Pulsar
from build_enterprise_signals import MakeSglPsrSignal
from sampler_output import sampler_output

def analyse_single_psr(psrname,
				  pathdirdata,
				  outdir,
				  model,
				  LTfit=10,
				  ephem=None,
				  noisedir=None,
				  sampler="ptmcmc",
				  Nsamples=1e6,
				  log_weights=None,
				  dlogz=0.01,
				  dynsample='rwalk',
				  pfrac=None,
				  Nthreads=None,
				  empdist_dir=None,
				  empdist_kde=False,
				  resume=False,
				  show_flags=None):
	###### Check Outdir
	if not os.path.isdir(outdir) and not show_flags:
		raise ValueError("%s doesn't exist."%outdir)

	###### Define the pulsars files
	basePath = pathdirdata+'/'+psrname+'/'+psrname
	print("\nParfile & timfile :")
	print(basePath+'.par')
	print(basePath+'_all.tim\n')

	###### Load the pulsars with libstempo
	print("\nRead ToAs with libstempo.")
	ltpsr = LT.tempopulsar(basePath+'.par', basePath+'_all.tim', ephem=ephem)
	
	if show_flags:
		show_flags = show_flags.split(",")
		print(ltpsr.flags())
		for flag in show_flags:
			flaglist = np.unique(ltpsr.flagvals(flag))
			print("\n%s:"%flag)
			print("-".join(flaglist))
			print("\n")
			for i,f in enumerate(flaglist):
				print("%s: %s"%(i,f))
		print("\n")
		exit()


	###### Fit TM (or not)
	for i in range(LTfit):
		if i==0: print("Fit %s times for the timing model."%LTfit)
		ltpsr.fit()

	###### Create enterprise pulsar objects
	print("\nMake enterprise pulsar object.")
	drop_t2pulsar=False if "TM_fit" in model else True # Save T2 pulsar object if fit for TM during MCMC
	psr = Pulsar(ltpsr, ephem=ephem, drop_t2pulsar=drop_t2pulsar)

	###### Make signal collection and models
	print("\nBuild enterprise signals and model.")

	model_list = model.split("/")
	if len(model_list)>1:
		print("\nHyperModel run with models: %s"%(", ".join(model_list)))
	
	model = []
	for i, m in enumerate(model_list):
		if len(model_list)>1: print("\n##### Model %s:"%(i+1))
		s = MakeSglPsrSignal(psr, m)
		model.append(s(psr))

	###### Build PTA object, set output directory and sample
	sampler_output(model=model, 
				sampler=sampler, 
				outdir=outdir, 
				noisedir=noisedir, 
				Nsamples=Nsamples, 
				log_weights=log_weights, 
				dlogz=dlogz, 
				dynsample=dynsample, 
				pfrac=pfrac, 
				Nthreads=Nthreads,
				empdist_dir=empdist_dir, 
				empdist_kde=empdist_kde, 
				resume=resume)

#####################################
## MAIN
# TODO: 
#   - Add other samplers (Bilby, ...)
#   - Allow PT with PTMCMC.
#	- Add kde Jumps
#####################################
str_help=\
'''
########################
How to write noise models:
########################
Add signals separated with ','
For each signal, define arguments separated with '_'.
To use HyperModel to compare 2 or more models: Separate models with '/'
See list of signals and arguments below:

####### Timing model: TM
fit: Fit for TM. Marginalize if not (default=False).
par: Specify sub-list of parameters to fit. All parameters used if not specified (default=None).
sig: Factor used to define prior range of TM params. Prior = U(-sig*5, sig*5), (default=1).
# Ex: TM_par-F0-F1-DM-DM1-DM2

####### White noise: WN. Temponest wn type (EFAC**2 * ToAerr**2 + EQUAD**2)
fix: Fix WN parameters to noisedir values (default=False).
# Ex: WN_fix

####### Gaussian process red noise: RN
nb: Number of freq. bins (default: 30). 
idx: Chromaticity. None for achromatic, 2 for DM var., 4 for scattering var., free for free-chromatic noise (prior=U(0,10)), (default=None).
psd: Choose the type of psd. pl for simple powerlaw, bpl for broken-power law, fs for free-spectrum (default=pl).
log: Logspaced bins (default=None).
dp: Dropout (only avaliable for powerlaw and achromatic RN), (default=None).
fn & fv: Flag name and value. Used to only select ToAs with particular flags (e.g., for Band/System noise), (default=None).
# Ex: RN_nb-30_log, RN_nb-30_psd-fs

####### Deterministic signals
## Exponential dip: E
ti: Initial time
tf: Final time
idx: Chromaticity. None for 2 (DM), other number to fix, free for free parameter (prior=U(0,10)), (default=2)
# Ex: E_ti-55000_tf-57000

#### Yearly chromatic noise: Y
idx: Chromaticity. None for 2 (DM), other number to fix, free for free parameter (prior=U(0,10)), (default=2)
# Ex: Y_idx-free

########################
EXAMPLES
########################

### J1600-3053, TM marginalized, WN (EFAC + TNEQUAD), RN30, DM100 sampled with PTMCMC with 1e6 iterations:
singularity exec ../EPTA_ENTERPRISE_220218_branch15_171336dd.sif python3 src/single_psr_analysis.py -o playground/examples/outdir/ -p playground/examples/toas/ -m TM,WN,RN_nb-30,RN_idx-2_nb-100 J1600-3053

### J1600-3053, TM marginalized, WN (EFAC + TNEQUAD), RN30, DM100 sampled with static Dynesty with 4000 livepoints, using multiprocessing with 8 threads:
# WARNING: Use 'export OMP_NUM_THREADS=1' in terminal before
singularity exec ../EPTA_ENTERPRISE_220218_branch15_171336dd.sif python3 src/single_psr_analysis.py -o playground/examples/outdir/ -p playground/examples/toas/ -m TM,WN,RN_nb-30,RN_idx-2_nb-100 --sampler Mdynesty --Nsamples 4000 --Nthreads 8 J1600-3053

'''

if __name__ == '__main__':
	scriptPath = os.path.realpath(__file__)

	parser = argparse.ArgumentParser(description="Single-psr analysis run", epilog=str_help,
		formatter_class=argparse.RawTextHelpFormatter)

	parser.add_argument("-p", "--pathdirdata",
		type=str, dest='pathdirdata', required=True,
		help="Path to the directory containing the data [required]")

	parser.add_argument("-o", "--outdir",
		type=str, dest='outdir', required=True,
		help="Path to the output directory [required]")

	parser.add_argument("-m", "--model",
		type=str, dest='model', required=True, 
		help="String used to set the single-pulsar noise model. Separate models with '/' for HyperModel [required]")

	parser.add_argument("--LTfit",
		type=int, dest='LTfit', default=10,
		help="Number of fits of the pulsar model, with Libstempo. 0 for no fitting [10 by default]")

	parser.add_argument("--ephem",
		type=str, dest='ephem', default=None,
		help="Solar-system ephemeris [None by default]")

	parser.add_argument("--noisedir",
		type=str, dest='noisedir', default=None,
		help="Directory with Json noisefiles to fix WN params [None by default]")

	parser.add_argument("--sampler",
		type=str, dest='sampler', default="ptmcmc",
		help="Sampler: 'ptmcmc', 'dynesty' or 'mc3'. For dynesty : dynesty, Mdynesty, ddynesty or Mddynesty [ptmcmc by default]")

	parser.add_argument("--Nsamples",
		type=float, dest='Nsamples', default=1e6,
		help="Number of samples [default: 1e6]")

	parser.add_argument("--log_weights",
		type=str, dest='log_weights', default=None, 
		help="List of values added to log_likelihood of each nmodel. Ex : 2,3 [None by default]")

	parser.add_argument("--dlogz",
		type=float, dest='dlogz', default=0.01,
		help="dlogz threshold for dynesty [0.01 by default]")

	parser.add_argument("--dynsample",
		type=str, dest='dynsample', default="rwalk",
		help="Sampling method for dynesty (rwalk, slice, rslice, hslice, unif, auto) [rwalk by default]")

	parser.add_argument("--pfrac",
		type=float, dest='pfrac', default=None,
		help="Posterior priority fraction for dynamic dynesty [None by default]")

	parser.add_argument("--Nthreads",
		type=int, dest="Nthreads", default=None,
		help="Define number of threads used for Mdynesty [1 by default]")

	parser.add_argument("--empdist_dir",
		type=str, dest='empdist_dir', default=None,
		help="Path to the empirical distribution pickle [None by default]")

	parser.add_argument("--empdist_kde",
		action="store_true", dest='empdist_kde', default=False,
		help="Activate HyperKDE method (TODO) [None by default]")

	parser.add_argument("--resume",
		action="store_true", dest="resume", default=False,
		help="Restart the sampling from the chain of the previous run (chain1.txt). [off by default]")

	parser.add_argument("--show_flags",
		type=str, dest="show_flags", default=None,
		help="Display all value available for the given flag, useful to see all systems. [None by default]")

	options, args = parser.parse_known_args()

	if len(args) != 1:
		parser.error("You must specify one pulsar name (see --help)!")
	
	psrname = args[0]

	print("Python command: %s"%(" ".join(sys.argv)))

	analyse_single_psr(psrname=psrname,
				  pathdirdata=options.pathdirdata,
				  outdir=options.outdir,
				  model=options.model,
				  LTfit=options.LTfit,
				  ephem=options.ephem,
				  noisedir=options.noisedir,
				  sampler=options.sampler,
				  Nsamples=options.Nsamples,
				  log_weights=options.log_weights,
				  dlogz=options.dlogz,
				  dynsample=options.dynsample,
				  pfrac=options.pfrac,
				  Nthreads=options.Nthreads,
				  empdist_dir=options.empdist_dir,
				  empdist_kde=options.empdist_kde,
				  resume=options.resume,
				  show_flags=options.show_flags)
