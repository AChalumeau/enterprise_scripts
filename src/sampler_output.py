import os
import glob
import json
import subprocess
import numpy as np
from enterprise.signals import signal_base
from enterprise_extensions.sampler import JumpProposal, save_runtime_info
from enterprise_extensions import hypermodel
from PTMCMCSampler.PTMCMCSampler import PTSampler as ptmcmc
import dynesty

try:
	import MCMC_multichain.MCMC_multichain as mcmc
except:
	print("Warning : MCMC_multichain not found !")

try :
	from mpi4py import MPI
	mpi4py_dbg = True
except:
	print("Warning : mpi4py not found !")
	mpi4py_dbg = False

def set_noisevals_from_noisedir(pta, noisedir, outdir):
	outnoisedir = outdir+"/noisefiles"
	if not os.path.isdir(outnoisedir):
		subprocess.run(["mkdir", outnoisedir])
	
	print("\nSet constant parameters from %s."%noisedir)
	params = {}

	for psrname in np.sort(pta.pulsars):
		print(noisedir+"/*%s*.json"%psrname)
		nf = glob.glob(noisedir+"/*%s*.json"%psrname)[0]
		with open(nf, 'r') as fin:
			param = json.load(fin)
			params.update(param)
		
		## Save noisefile in output directory
		with open(outnoisedir+"/"+os.path.basename(nf), 'w') as f:
			json.dump(param, f, indent=4)

	pta.set_default_params(params)

def set_noisevals_from_vals(pta, fixpars, outdir):
	outnoisedir = outdir+"/noisefiles"
	if not os.path.isdir(outnoisedir):
		subprocess.run(["mkdir", outnoisedir])
	
	fixpars = fixpars.split(",")

	print("\nSet constant parameters: %s."%fixpars)
	params = {}
	
	for psrname in np.sort(pta.pulsars):
		for i,f in enumerate(fixpars):
			parn, parv = fixpars[i].split("#")[0], float(fixpars[i].split("#")[1])
			param = {"%s_%s"%(psrname,parn): parv}
			params.update(param)

			## Save noisefile in output directory
			with open(outnoisedir+"/"+"noisefile_"+psrname+".json", 'w') as f:
				json.dump(param, f, indent=4)

	pta.set_default_params(params)

def sampler_output(model,
				sampler, 
				outdir, 
				noisedir=None,
				fixpars=None,
				Nsamples=1e6,
				log_weights=None,
				dlogz=0.01,
				dynsample='rwalk',
				pfrac=None,
				Nthreads=None,
				resume=False, 
				empdist_dir=None, 
				empdist_kde=None):
	if sampler in ["dynesty","Mdynesty","ddynesty","Mddynesty"]:
		global pta

	###### HyperModel
	#if len(model)>1:
	if isinstance(model[0], list):
		###### Make PTA object
		pta = dict.fromkeys(np.arange(0, len(model)))
		for i, m in enumerate(model):
			pta[i] = signal_base.PTA(m)
			
			###### Set values to constant params (WN) from noisedir if there are.
			if noisedir:
				set_noisevals_from_noisedir(pta=pta[i], noisedir=noisedir, outdir=outdir)
			elif fixpars:
				set_noisevals_from_vals(pta, fixpars)


		if log_weights:
			log_weights = [float(i) for i in log_weights.split(",")]
			print("\nLog-weights :")
			for i, m in enumerate(model):
				print("%s : %s"%(m, log_weights[i]))
		else:
			print("\nNo log-weight")
		
		###### Make HyperModel object
		hyper_model = hypermodel.HyperModel(pta, log_weights=log_weights)

		###### Sampling
		HyperModel_launcher(hyper_model=hyper_model, outdir=outdir, Nsamples=Nsamples, resume=resume)

	###### Single noise model
	else:
		pta = signal_base.PTA(model)
		
		###### Set values to constant params (WN) from noisedir if there are.
		if noisedir:
			set_noisevals_from_noisedir(pta=pta, noisedir=noisedir, outdir=outdir)
		elif fixpars:
			set_noisevals_from_vals(pta, fixpars, outdir=outdir)
			
		# ## Print prior and LH of a random param vector
		# np.random.seed(10)
		# par_rand = np.hstack([p.sample() for p in pta.params])
		# # idx = [i for i,p in enumerate(pta.param_names) if "drop" in p][0]
		# # par_rand[idx] = 0.2
		# pr_rand = pta.get_lnprior(par_rand)
		# lh_rand = pta.get_lnlikelihood(par_rand)
		# print('\nRand param vector = ', list(par_rand))
		# print('logPrior = ', pr_rand)
		# print('logLH = ', lh_rand, '\n')
		# exit()

		###### Save param names and priors
		parnames = []
		idbg = 0
		for p in pta.param_names:
			if not "timing_model_tmparams" in p:
				parnames.append(p)
			else:
				parnames.append(psr.t2pulsar.pars()[idbg])
				idbg+=1

		np.savetxt(outdir+'/pars.txt', parnames, fmt='%s')
		np.savetxt(outdir+'/priors.txt', pta.params, fmt='%s')

		###### Sampling
		print("\n### Fitted Parameters")
		for i,p in enumerate(pta.params):
			print("{0:>3d} : {1:s}".format(i,str(p)))

		print("\nPrepare sampler.")
		save_runtime_info(pta, outdir=outdir, human=None)
		if sampler == "ptmcmc":
			ptmcmc_launcher(pta=pta, outdir=outdir, Nsamples=Nsamples, resume=resume, empdist_dir=empdist_dir, empdist_kde=empdist_kde)

		if sampler in ["dynesty","Mdynesty"]:
			static_dynesty_launcher(pta=pta, outdir=outdir, sampler=sampler, Nlivepoints=Nsamples, dlogz=dlogz, dynsample=dynsample, Nthreads=Nthreads)
		
		if sampler in ["ddynesty","Mddynesty"]:
			dynamic_dynesty_launcher(pta=pta, outdir=outdir, sampler=sampler, Nlivepoints=Nsamples, dlogz=dlogz, dynsample=dynsample, pfrac=pfrac, Nthreads=Nthreads)

		if sampler=="mc3" :
			mc3_launcher(pta=pta, outdir=outdir, Nsamples=Nsamples)

def HyperModel_launcher(hyper_model, outdir, Nsamples=1e6, resume=None):
	Nsamples = int(Nsamples)
	print("\nSampler : ptmcmc with %s iterations."%Nsamples)

	sp = hyper_model.setup_sampler(resume=resume, outdir=outdir)
	x0 = hyper_model.initial_sample()
	sp.sample(x0, Nsamples, SCAMweight=30, AMweight=15, DEweight=50)

	# Print scores
	print("\nGet nmodel scores :\n")
	ch=np.loadtxt(outdir+"/chain_1.txt")
	ch=ch[int(len(ch)*.3):]
	pars=list(np.loadtxt(outdir+"/pars.txt",dtype=str))

	# idx = pars.index("nmodel")
	# nmod_vals = np.arange(-0.5,len(sModels_list))
	# for i,sModel in enumerate(sModels_list):
	#     nvali = nmod_vals[i]
	#     nvalf = nmod_vals[i+1]    
	#     mod_val = len(np.where((ch[:,idx]>nvali) & (ch[:,idx]<nvalf))[0]) / len(ch)
	#     print("%s : %.5f"%(sModel, mod_val))

def ptmcmc_launcher(pta, outdir, Nsamples=1e6, resume=False, empdist_dir=None, empdist_kde=None):
	Nsamples = int(Nsamples)
	print("\nSampler : ptmcmc with %s iterations."%Nsamples)

	###### set-up the sampler
	x0 = np.hstack([p.sample() for p in pta.params])
	ndim = len(x0)
	
	#print(pta.get_lnlikelihood(x0))
	
	###### initial jump covariance matrix
	cov = np.diag(np.ones(ndim) * 0.01**2) # helps to tune MCMC proposal distribution

	###### sampler object
	if not mpi4py_dbg:
		sampler = ptmcmc(ndim, pta.get_lnlikelihood, pta.get_lnprior, cov, outDir=outdir, resume=resume)
	else:	
		sampler = ptmcmc(ndim, pta.get_lnlikelihood, pta.get_lnprior, cov, outDir=outdir, comm=MPI.COMM_WORLD, resume=resume)
	
	###### Set Jump proposals
	empdist_pars = None
	Add_sglpsr_Jump_proposals(pta=pta, sampler=sampler, outdir=outdir, empdist_dir=empdist_dir, empdist_pars=empdist_pars, empdist_kde=empdist_kde)

	###### sampling
	#sampler.sample(x0, N, SCAMweight=30, AMweight=15, DEweight=50)
	#sampler.sample(x0, N, SCAMweight=40, DEweight=60)
	if not mpi4py_dbg:
		sampler.sample(x0, Nsamples, SCAMweight=40, DEweight=60)
	else:
		sampler.sample(x0, Nsamples, Tmin=1., Tmax=4., SCAMweight=40, DEweight=60, writeHotChains=True)


def static_dynesty_launcher(pta, outdir, sampler="dynesty", Nlivepoints=1e3, dlogz=0.01, dynsample='rwalk', Nthreads=None):
	Nlivepoints = int(Nlivepoints)

	###### Set Pool if multiprocessing
	if sampler=="dynesty":
		ppp = None
		print("\nSampler : Static dynesty with %s live points, dlogz = %s."%(Nlivepoints, dlogz))
	elif sampler=="Mdynesty":
		from multiprocessing import Pool, cpu_count
		# Nthreads = cpu_count()-1
		ppp = Pool(Nthreads)
		print("\nSampler : Static dynesty with %s live points, dlogz = %s, using multiprocessing with %s threads."%(Nlivepoints, dlogz, Nthreads))

	###### Prepare sampler
	par_nms = pta.param_names
	Npar = len(par_nms)

	sampler = dynesty.NestedSampler(log_l, TransformPrior, ndim=Npar, nlive=Nlivepoints, \
										bound='multi', sample=dynsample, pool=ppp, queue_size=Nthreads)
										# bound='multi', sample='rwalk', periodic=[29,30,31])
										# bound='multi', sample='slice', periodic=[6])
										# bound='multi', sample='auto', periodic=[6])
	
	###### sampling
	chkpts_maxC = [1000000, None]
	for maxC in chkpts_maxC:
		sampler.run_nested(dlogz=dlogz, maxcall=maxC)
						 # , mincall=10000, \
						 # saveState=1000, saveFile="/home/stas/Projects/PTA/data-epta/tutorials/Results/1909_M3/DynastyLP.dat")
		res = sampler.results
		with open(outdir+'DynRes.pkl', 'wb') as res_file:
			pickle.dump(res, res_file)
		
		print("\n")
		res.summary()
		print("\n")

def dynamic_dynesty_launcher(pta, outdir, sampler="ddynesty", Nlivepoints=1e3, dlogz=0.01, pfrac=None, Nthreads=None):
	Nlivepoints = int(Nlivepoints)

	###### Set Pool if multiprocessing
	if sampler=="ddynesty":
		ppp = None
		print("\nSampler : Dynamic dynesty with %s live points, dlogz = %s, pfrac=%s."%(Nlivepoints, dlogz, str(pfrac)))
	elif sampler=="Mddynesty":
		from multiprocessing import Pool, cpu_count
		# Nthreads = cpu_count()-1
		ppp = Pool(Nthreads)
		print("\nSampler : Dynamic dynesty with %s live points, dlogz = %s, pfrac=%s, using multiprocessing with %s threads."%(Nlivepoints, dlogz, str(pfrac), Nthreads))

	###### Prepare sampler
	par_nms = pta.param_names
	Npar = len(par_nms)

	if pfrac:
		wt_kwargs={'pfrac': pfrac}
		stop_kwargs={'pfrac': pfrac}
	else:
		wt_kwargs=None
		stop_kwargs=None

	sampler = dynesty.DynamicNestedSampler(log_l, TransformPrior, ndim=Npar,\
										bound='multi', sample=dynsample, pool=ppp, queue_size=Nthreads)
										# bound='multi', sample='rwalk', periodic=[29,30,31])
										# bound='multi', sample='slice', periodic=[6])
										# bound='multi', sample='auto', periodic=[6])
	
	###### sampling
	chkpts_maxC = [1000000, None]
	for maxC in chkpts_maxC:
		sampler.run_nested(dlogz_init=dlogz, maxcall=maxC, nlive_init=Nlivepoints, nlive_batch=100, wt_kwargs=wt_kwargs, stop_kwargs=stop_kwargs)
						 # , mincall=10000, \
						 # saveState=1000, saveFile="/home/stas/Projects/PTA/data-epta/tutorials/Results/1909_M3/DynastyLP.dat")
		res = sampler.results
		with open(outdir+'DynRes.pkl', 'wb') as res_file:
			pickle.dump(res, res_file)
		
		print("\n")
		res.summary()
		print("\n")

def mc3_launcher(pta, outdir, Nsamples=1e5):
	Nsamples = int(Nsamples)
	print("\nSampler : MCMC_multichain with %s iterations."%Nsamples)
	
	###### Prepare sampler
	Nchains = 4
	xs = []
	for i in range(Nchains):
		x0 = np.hstack(p.sample() for p in pta.params)
		ndim = len(x0)
		print("Initial value of the",ndim,"parameters",x0)
		xs.append(x0)
		print ('ln_prior=', pta.get_lnprior(x0), 'ln_like=', pta.get_lnlikelihood(x0) )
	# pta.get_lnlikelihood, pta.get_lnprior

	priors = []
	for each in pta.params:
		bnd = [each.prior.func_kwargs['pmin'], each.prior.func_kwargs['pmax']]
		priors.append(bnd)

	#print (priors)
	#print (pta.param_names)  
	Nadapt=500
	prop_args = {'cov' : None, 'DE_skip': 1000}

	Ms = mcmc.MultiChain_MCMC(np.array(priors), Nchains, log_pr, Nadapt, pta.param_names, 1 )
	
	###### sampling
	Ms.InitialPoints(xs)
	Ms.InitializeProposals([{'SCAM':70., 'DE':30, 'slice':40.}, \
							{'SCAM':70., 'DE':50., 'slice':40.},\
							{'SCAM':70., 'DE':50., 'slice':40.},\
							{'slice':70., 'DE_all':30.}], **prop_args)
	chan = Ms.runMCMC(Nsamples, verbal=True, printN=5000, outdir=outdir)
	gr = Ms.EvaluateGelmanRubinRatio(chan, Nsamples/2)
	np.savetxt(outdir+"/GelmanRubinRatio.txt",gr)

def Add_sglpsr_Jump_proposals(pta, sampler, outdir, empdist_dir=None, empdist_pars=None, empdist_kde=False):
	#empdist_dir = "/media/aurelienchalumeau/Samsung_T5/chains/data-epta2106020045eafe_NUPPIL_SB_Jumps/J1909-3744/M_RN10_DM100/ptmcmc_nsamp_2000000"
	if empdist_dir:
		if not empdist_kde:
			params = list(np.loadtxt(empdist_dir+"/pars.txt", dtype=str))
			chain = np.loadtxt(empdist_dir+"/chain_1.txt")
			empdist_name = outdir+'/emp_distr.pkl'
			model_utils.make_empirical_distributions(empdist_pars, params, chain, burn=int(len(chain)*.3), nbins=41, filename=empdist_name)
	else:
		empdist_name = None

	print("\nSet Jump proposals.\n")

	# additional jump proposals
	jp = JumpProposal(pta, empirical_distr=empdist_name)

	# always add draw from prior
	sampler.addProposalToCycle(jp.draw_from_prior, 5)

	# try adding empirical proposals
	if empdist_dir:
		if not empdist_kde:
			print('Adding empirical proposals.')
			sampler.addProposalToCycle(jp.draw_from_empirical_distr, 25)

	sel_sig = ["rn", "dm_gp", "fcn", "chrom-rn", "srn", "dm_srn", "freechrom-srn", "chrom-srn", 
			"dm-expd", "freechrom-expd", "chrom-expd", 
			"dm-y", "freechrom-y", "chrom-y",
			"gw"]
	for s in sel_sig:
		if any([s in p for p in pta.param_names]):
			#pnames = [p.name for p in pta.params if s in p.name]
			#print('Adding %s prior draws with parameters :'%s, pnames, '\n')
			print('Adding %s prior draws.'%s)
			sampler.addProposalToCycle(jp.draw_from_par_prior(s), 10)
	
	# Ephemeris prior draw
	if 'd_jupiter_mass' in pta.param_names:
		print('Adding ephemeris model prior draws...\n')
		sampler.addProposalToCycle(jp.draw_from_ephem_prior, 10)

	# GWB uniform distribution draw
	if 'gw_log10_A' in pta.param_names:
		print('Adding GWB uniform distribution draws...\n')
		sampler.addProposalToCycle(jp.draw_from_gwb_log_uniform_distribution, 10)

	# Dipole uniform distribution draw
	if 'dipole_log10_A' in pta.param_names:
		print('Adding dipole uniform distribution draws...\n')
		sampler.addProposalToCycle(jp.draw_from_dipole_log_uniform_distribution, 10)

	# Monopole uniform distribution draw
	if 'monopole_log10_A' in pta.param_names:
		print('Adding monopole uniform distribution draws...\n')
		sampler.addProposalToCycle(jp.draw_from_monopole_log_uniform_distribution, 10)

def log_l(x):
	return pta.get_lnlikelihood(x)

def log_pr(x):
	return (pta.get_lnlikelihood(x) + pta.get_lnprior(x))

def TransformPrior(theta, verbose=False):
	if verbose:
		print('\n', len(theta), '\n')
	ph_pars = np.zeros(len(theta))
	idx_offset = 0
	for i, nm in enumerate(pta.params):
		if nm.size and nm.size>1:
			for j in range(nm.size):
				ph_pars[i+idx_offset] = (pta.params[i].prior.func_kwargs['pmax'] - pta.params[i].prior.func_kwargs['pmin']) * theta[i+idx_offset] + pta.params[i].prior.func_kwargs['pmin']
				idx_offset+=1
		else:
			ph_pars[i+idx_offset] = ( pta.params[i].prior.func_kwargs['pmax'] - pta.params[i].prior.func_kwargs['pmin'])*theta[i+idx_offset] + pta.params[i].prior.func_kwargs['pmin']
		if verbose:
			print(nm, pta.params[i].prior.func_args, pta.params[i].prior.func_kwargs['pmin'], pta.params[i].prior.func_kwargs['pmax'])
	return ph_pars